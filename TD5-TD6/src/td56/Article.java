/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td56;

/**
 *
 * @author xehx
 */
public abstract class Article {
    
    private String marque;
    private double prix;
    private int quantité;

    public Article(String marque, double prix, int quantité) {
        this.marque = marque;
        this.prix = prix;
        this.quantité = quantité;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getMarque() {
        return marque;
    }

    public int getQuantité() {
        return quantité;
    }

    public static String slogan(){
        return "Nos articles sont les meilleurs.";
    }

    @Override
    public String toString() {
        return "marque=" + marque + ", prix=" + prix + ", quantité=" + quantité;
    }
    
    public abstract double calculerRemise();
    
}
