/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td56;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author xehx
 */
public class TD5 {

    /**
     * @param args the command line arguments
     */
    private static final ArrayList<ChaussureDeSport> chaussureDeSports = new ArrayList<>();

    public static void main(String[] args) {
        
        ChaussureDeSport cs1 = new ChaussureDeSport("Hoka One One", 120, 5, 42.5, "ATR5");
        ChaussureDeSport cs2 = new ChaussureDeSport("Salomon", 180, 3, 44, "S-Lab Sense 8");
        ChaussureDeSport cs3 = new ChaussureDeSport("Asics", 140, 2, 43, "Fuji Trabuco");
        ChaussureDeSport cs4 = new ChaussureDeSport("Asics", 135, 5, 41.5, "GT-2000");
        ChaussureDeSport cs5 = new ChaussureDeSport("Salomon", 130, 4, 40.5, "SpeedCross 5");
        chaussureDeSports.addAll(Arrays.asList(cs1, cs2, cs3, cs4, cs5));
        Collections.sort(chaussureDeSports);
//        chaussureDeSports.forEach(c -> System.out.println("designation="+c.getDesignation()+", remise="+c.calculerRemise()+"%"));
//        System.out.println("-----------------");
//        cs5.setPrix(120);
//        Collections.sort(chaussureDeSports);
//        chaussureDeSports.forEach(c -> System.out.println("designation="+c.getDesignation()+", remise="+c.calculerRemise()+"%"));
        
        IHMC ihmc = new IHMC();
        ihmc.setVisible(true);
        ihmc.setListe(chaussureDeSports);
        ihmc.affichage();
    }
    
    //VERSION EDWIN
    public static final ChaussureDeSport recherche(String designation){
        return chaussureDeSports.stream().filter(c -> c.getDesignation().equals(designation)).findAny().orElse(null);
    }
    
}
