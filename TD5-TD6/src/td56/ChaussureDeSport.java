/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td56;

/**
 *
 * @author xehx
 */
public class ChaussureDeSport extends Article implements Personnalisable, Comparable {
    
    private String designation;
    private double pointure;
    public static int nbChaussures = 0;

    public ChaussureDeSport(String marque, double prix, int quantité, double pointure, String designation) {
        super(marque, prix, quantité);
        this.designation = designation;
        this.pointure = pointure;
        nbChaussures += quantité;
    }

    public static int getNbChaussures() {
        return nbChaussures;
    }

    @Override
    public String toString() {
        return super.toString()+", designation=" + designation + ", pointure=" + pointure;
    }

    @Override
    public double calculerRemise() {
        if(getMarque().equals("Hoka One One") && designation.equals("ATR5")) return 10;
        if(getMarque().equals("Salomon") && designation.equals("S-Lab Sense 8")) return 20;
        return 0;
    }

    public String getDesignation() {
        return designation;
    }

    public double getPointure() {
        return pointure;
    }

    @Override
    public double surCout(int nbArcticle) {
        return nbArcticle > seuilRentabilite ? 0 : 30;
    }

    @Override
    public int compareTo(Object o) {
        if(getPrix() == ((ChaussureDeSport) o).getPrix()) return Double.compare(getQuantité(), ((ChaussureDeSport) o).getQuantité());
        return getPrix() > ((ChaussureDeSport) o).getPrix() ? 1 : -1;
    }

}
