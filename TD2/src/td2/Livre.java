/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td2;

/**
 *
 * @author xehx
 */
public class Livre {
    private String titre;
    private String genre;

    public Livre(String titre, String genre) {
        this.titre = titre;
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "titre = "+this.getTitre()+", genre = "+this.getGenre();
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
