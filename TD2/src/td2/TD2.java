/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td2;

/**
 *
 * @author xehx
 */
public class TD2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Livre[] livres = new Livre[5];
        Livre l1 = new Livre("l, Robot", "Science Fiction");
        Livre l2 = new Livre("l0 petits nègres", "Policier");

        livres[0] = l1; livres[1] = l2;
        livres[2] = new Livre("World War Z", "Science-Fiction");
        livres[3] = new Livre("Mort sur le Nil", "Policier");
        livres[4] = new Livre("Prométhée", "Mythologie");

        for(int i = 0; i < 5; i++){
            System.out.println(livres[i].toString());
        }
        
        IHMBibliotheque ihm = new IHMBibliotheque(livres);
        ihm.setVisible(true);
        
    }
    
}
