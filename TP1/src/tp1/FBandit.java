/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

import java.awt.Frame;
import java.util.Random;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author ehelet
 */
public class FBandit extends javax.swing.JDialog {

    /**
     * Creates new form FBandit
     */
    private JLabel[] chiffres;
    private int nbWin = 0;
    
    public FBandit(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        chiffres = new JLabel[]{jLabelChiffre1, jLabelChiffre2, jLabelChiffre3};
        rbFacile.setSelected(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgNiveau = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabelChiffre1 = new javax.swing.JLabel();
        jLabelChiffre2 = new javax.swing.JLabel();
        jLabelChiffre3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        rbFacile = new javax.swing.JRadioButton();
        rbMoyen = new javax.swing.JRadioButton();
        rbDifficile = new javax.swing.JRadioButton();
        rbTDifficile = new javax.swing.JRadioButton();
        rbTTDifficile = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        nbLabelWin = new javax.swing.JLabel();
        btnRetour = new javax.swing.JButton();
        btnJouer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("JEU DU BANDIT MANCHOT");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/bandit.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabelChiffre1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabelChiffre1.setText("0");

        jLabelChiffre2.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabelChiffre2.setText("0");

        jLabelChiffre3.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabelChiffre3.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabelChiffre1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addComponent(jLabelChiffre2)
                .addGap(40, 40, 40)
                .addComponent(jLabelChiffre3)
                .addGap(39, 39, 39))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelChiffre1)
                    .addComponent(jLabelChiffre2)
                    .addComponent(jLabelChiffre3))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel6.setText("Niveau de jeu");

        bgNiveau.add(rbFacile);
        rbFacile.setText("Facile");
        rbFacile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbFacileActionPerformed(evt);
            }
        });

        bgNiveau.add(rbMoyen);
        rbMoyen.setText("Moyen");

        bgNiveau.add(rbDifficile);
        rbDifficile.setText("Difficile");

        bgNiveau.add(rbTDifficile);
        rbTDifficile.setText("Très difficile");
        rbTDifficile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTDifficileActionPerformed(evt);
            }
        });

        bgNiveau.add(rbTTDifficile);
        rbTTDifficile.setText("Très Très Difficile");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(71, 71, 71))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbTTDifficile)
                    .addComponent(rbTDifficile)
                    .addComponent(rbDifficile)
                    .addComponent(rbMoyen)
                    .addComponent(rbFacile))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbFacile)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbMoyen)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbDifficile)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbTDifficile)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbTTDifficile)
                .addContainerGap())
        );

        jLabel7.setText("Nombre de victoire :");

        nbLabelWin.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        nbLabelWin.setText("0");

        btnRetour.setText("RETOUR");
        btnRetour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRetourActionPerformed(evt);
            }
        });

        btnJouer.setText("JOUER");
        btnJouer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJouerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(nbLabelWin, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRetour)
                .addGap(21, 21, 21))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(106, 106, 106))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(btnJouer)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnJouer))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(nbLabelWin)
                    .addComponent(btnRetour))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbFacileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbFacileActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbFacileActionPerformed

    private void btnRetourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRetourActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        getParent().setVisible(true);
    }//GEN-LAST:event_btnRetourActionPerformed

    private void btnJouerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJouerActionPerformed
        // TODO add your handling code here:
        jouer();
    }//GEN-LAST:event_btnJouerActionPerformed

    private void rbTDifficileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTDifficileActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbTDifficileActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgNiveau;
    private javax.swing.JButton btnJouer;
    private javax.swing.JButton btnRetour;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabelChiffre1;
    private javax.swing.JLabel jLabelChiffre2;
    private javax.swing.JLabel jLabelChiffre3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel nbLabelWin;
    private javax.swing.JRadioButton rbDifficile;
    private javax.swing.JRadioButton rbFacile;
    private javax.swing.JRadioButton rbMoyen;
    private javax.swing.JRadioButton rbTDifficile;
    private javax.swing.JRadioButton rbTTDifficile;
    // End of variables declaration//GEN-END:variables

    public void jouer() {
        int niveau = rbFacile.isSelected() ? 2 : rbMoyen.isSelected() ? 4 : rbDifficile.isSelected() ? 6 : rbTDifficile.isSelected() ? 100 : 1000000000;
        Random r = new Random();
        
        int[] res = new int[3];
        for(int i = 0; i < 3; i++){
            int n = r.nextInt(niveau);
            chiffres[i].setText(n+"");
            res[i] = n;
        }
        
        if(res[0] == res[1] && res[1] == res[2]){
            nbWin++;
            nbLabelWin.setText(nbWin+"");
            JOptionPane.showMessageDialog(this, "Vous avez gagné !");
        }
        
    }
}
