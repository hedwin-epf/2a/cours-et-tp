/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author ehelet
 */
public class IHMPrincipal extends JFrame{

    private FBandit fichBandit;
    private FDessin fichDessin;
    
    public IHMPrincipal() {
        this.setTitle("EPF");
        this.setSize(600, 400);
        
        
        JButton jouer = new JButton("JOUER");
        jouer.setFont(new Font("Arial", 0, 36));
        jouer.addActionListener(evt -> {
            this.setVisible(false);
            fichBandit.setVisible(true);
        });
        
        JButton dessiner = new JButton("DESSINER");
        dessiner.setFont(new Font("Arial", 0, 36));
        dessiner.addActionListener(evt -> {
            this.setVisible(false);
            fichDessin.setVisible(true);
        });
        
        JButton quitter = new JButton("QUITTER");
        quitter.setFont(new Font("Arial", 0, 36));
        quitter.addActionListener(evt -> {
            System.exit(0);
        });
        
        
        JLabel bvn = new JLabel("BIENVENUE", SwingConstants.CENTER);
        bvn.setFont(new Font("Arial", 0, 36));
        
        JLabel img = new JLabel(new ImageIcon("src/images/epf.png"));
        
        add(bvn, BorderLayout.NORTH);
        add(img, BorderLayout.CENTER);
        
        JPanel panel = new JPanel();
        panel.add(jouer, BorderLayout.WEST);
        panel.add(dessiner, BorderLayout.CENTER);
        panel.add(quitter, BorderLayout.EAST);
        
        add(panel, BorderLayout.SOUTH);
        
        fichBandit = new FBandit(this, false);
        fichDessin = new FDessin(this, false);
        
    }  
    
    
    
}
