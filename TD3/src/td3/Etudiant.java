package td3;

public class Etudiant {

    private String nom;
    private double[] notes = new double[50];
    private int nbNotes = 0;

    public Etudiant(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void addNotes(double... notes){
        for(int i = 0; i < notes.length; i++) this.notes[i+nbNotes] = notes[i];
        nbNotes += notes.length;
    }

    public int getNbNotes() {
        return nbNotes;
    }

    public double getMoyenne(){
        double sum = 0;
        for(int i = 0; i < nbNotes; i++) sum += notes[i];
        return sum/nbNotes;
    }

}
