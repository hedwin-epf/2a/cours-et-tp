/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td3;

import java.util.Random;

/**
 *
 * @author xehx
 */
public class TD3 {

    
    public static void main(String[] args) {
//        EtudiantEPF etudiant = new EtudiantEPF("Edwin", '3');
//        etudiant.addNotes(15.5, 16, 17, 15, 10, 8, 2, 16, 20, 9, 6, 11, 13);
//        System.out.println("L'étudiant s'appelle : "+etudiant.getNom());
//        System.out.println("Sa moyenne est : "+etudiant.getMoyenne());
//        System.out.println("Son groupe est : "+etudiant.getGroupe());
        EtudiantEPF[] etudiants = new EtudiantEPF[25];
        Random random = new Random();
        for(int i = 0; i < etudiants.length; i++){
            etudiants[i] = new EtudiantEPF("e"+i, (char) (random.nextInt(4)+65));
            for(int j = 0; j < 5; j++){
                etudiants[i].addNotes(random.nextInt(21));
            }
        }
        
        
        IHM ihm = new IHM();
        ihm.setTableau(etudiants);
        ihm.affichage();
        ihm.setVisible(true);
    }
    
}
