package td3;

public class EtudiantEPF extends Etudiant {
    private char groupe;

    public EtudiantEPF(String nom, char groupe) {
        super(nom);
        this.groupe = groupe;
    }

    public char getGroupe() {
        return groupe;
    }

}
