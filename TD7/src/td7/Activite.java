package td7;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Activite {
    private String nom;
    private LinkedList<Abonne> listAbonnes = new LinkedList<>();

    public Activite(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void add(Abonne... abonnes){
        listAbonnes.addAll(Arrays.asList(abonnes));
    }

    public Abonne recherche(int numero){
        return listAbonnes.stream().filter(a -> a.testNumero(numero)).findAny().orElse(null);
    }

    public boolean testNom(String nom){
        return nom.equals(this.nom);
    }

    @Override
    public String toString() {
        return "nom="+nom+", listAbonnes=["+listAbonnes.stream().map(Abonne::toString).collect(Collectors.joining("; "))+"]";
    }

    public void triParNom(){
        Collections.sort(listAbonnes);
    }

}
