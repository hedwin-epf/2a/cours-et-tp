package td7;

import java.util.*;
import java.util.stream.Collectors;

public class Club {
    private HashSet<Abonne> ensAbonnes = new HashSet<>();
    private ArrayList<Activite> listActivites = new ArrayList<>();

    public void add(int numero, String nom){
        ensAbonnes.add(new Abonne(numero, nom));
    }

    public void add(String... nomActivite){
        listActivites.addAll(Arrays.stream(nomActivite).map(Activite::new).collect(Collectors.toList()));
    }

    public HashSet<Abonne> getListAbonnes() {
        return ensAbonnes;
    }

    public void addAbAc(int numero, String nomActivite) {
        listActivites.stream().filter(a -> a.testNom(nomActivite)).findAny().ifPresent(activite -> ensAbonnes.stream().filter(abo -> abo.testNumero(numero)).findAny().ifPresent(activite::add));
    }

    public LinkedList<Activite> presence(int numero){
        return listActivites.stream().filter(a -> a.recherche(numero) != null).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public String toString() {
        return "abonnes=["+ensAbonnes.stream().map(Abonne::toString).collect(Collectors.joining(";"))+"], " +
                "activities=["+listActivites.stream().map(Activite::toString).collect(Collectors.joining("; "))+"]";
    }
}
