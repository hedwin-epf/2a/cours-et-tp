package td7;

public class Abonne implements Comparable {
    private int numero;
    private String nom;

    public Abonne(int numero, String nom) {
        this.numero = numero;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "numero="+numero+", nom="+nom;
    }

    public boolean testNumero(int numero){
        return this.numero == numero;
    }

    @Override
    public int compareTo(Object o) {
        return nom.compareTo(((Abonne) o).nom);
    }
}
