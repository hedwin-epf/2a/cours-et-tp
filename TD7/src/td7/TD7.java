/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td7;

/**
 *
 * @author xehx
 */
public class TD7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Club club = new Club();
        club.add("Tennis", "Badminton", "Squash");
        club.add(123, "Pierre");
        club.add(456, "Nicolas");
        club.add(789, "Lucas");
        club.add(258, "Paul");
        club.addAbAc(123, "Tennis");
        club.addAbAc(789, "Tennis");
        club.addAbAc(456, "Badminton");
        club.addAbAc(258, "Badminton");
        club.addAbAc(123, "Squash");
        System.out.println(club.toString());
        club.presence(123).forEach(a -> System.out.println(a.getNom()));
    }
    
}
