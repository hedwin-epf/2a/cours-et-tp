/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

/**
 *
 * @author xehx
 */
public class Raquette extends Produit {
    
    private String sport;
    private String material;

    public Raquette(String sport, String material, String designation, String reference, double coutProduction) {
        super(designation, reference, coutProduction);
        this.sport = sport;
        this.material = material;
    }

    public String getSport() {
        return sport;
    }
    
    @Override
    public String toString() {
        return super.toString() + ", sport=" + sport + ", materiau=" + material; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
