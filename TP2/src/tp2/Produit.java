/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

/**
 *
 * @author xehx
 */
public class Produit {    
    
    private String designation;
    private String reference;
    private double coutProduction;

    public Produit(String designation, String reference, double coutProduction) {
        this.designation = designation;
        this.reference = reference;
        this.coutProduction = coutProduction;
    }

    public String getDesignation() {
        return designation;
    }

    public double getCoutProduction() {
        return coutProduction;
    }

    @Override
    public String toString() {
        return "designation=" + designation + ", référence=" + reference + ", coût de production=" + coutProduction;
    }    
    
}
