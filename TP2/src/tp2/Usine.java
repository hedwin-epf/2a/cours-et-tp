/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Filter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author xehx
 */
public class Usine {
    
    private final List<Produit> produits = new ArrayList<>();
    private String nom;

    public Usine(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
    
    public void addProduit(Produit... produits){
        this.produits.addAll(Arrays.asList(produits));
    }
    
    public void removeProduit(Produit... produits){
        this.produits.removeAll(Arrays.asList(produits));
    }

    @Override
    public String toString() {
        return produits.stream().map(Produit::getDesignation).collect(Collectors.joining("\n"));
    }
    
    public boolean recherche(String designation){
        return produits.stream().map(Produit::getDesignation).anyMatch(designation::equalsIgnoreCase);
    }
    
    public boolean recherche(Produit produit){
        return produits.stream().anyMatch(produit::equals);
    }
    
    public double getCoutMoyen(){
        return produits.stream().map(Produit::getCoutProduction).reduce(0D, Double::sum) / produits.size();
    }
    
    public double getCoutMoyen(Class<? extends Produit> type){
        List<Produit> ps = produits.stream().filter(type::isInstance).collect(Collectors.toList());
        return ps.stream().map(Produit::getCoutProduction).reduce(0D, Double::sum) / ps.size();
    }
    
    public double getCoutMoyenRaquette(String sport){
        List<Produit> ps = produits.stream().filter(Raquette.class::isInstance).map(Raquette.class::cast).filter(r -> r.getSport().equals(sport)).collect(Collectors.toList());
        return ps.stream().map(Produit::getCoutProduction).reduce(0D, Double::sum) / ps.size();
    }
    
}
