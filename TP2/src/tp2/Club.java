/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2;

/**
 *
 * @author xehx
 */
public class Club extends Produit {
    
    private String type;

    public Club(String type, String designation, String reference, double coutProduction) {
        super(designation, reference, coutProduction);
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString() + ", type=" + type; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
